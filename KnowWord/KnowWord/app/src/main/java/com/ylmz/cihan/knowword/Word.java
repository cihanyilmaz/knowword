package com.ylmz.cihan.knowword;

/**
 * Created by Cihan Yılmaz on 3.12.2018.
 */

public class Word {

    private String uid;
    private String turkishWord;
    private String englishWord;
    private String sentence;


    public Word(String turkishWord, String englishWord, String sentence, String uid)
    {
        this.uid=uid;
        this.turkishWord=turkishWord;
        this.englishWord=englishWord;
        this.sentence=sentence;

    }

    public Word()
    {


    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTurkishWord() {
        return turkishWord;
    }

    public void setTurkishWord(String turkishWord) {
        this.turkishWord = turkishWord;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }


}

package com.ylmz.cihan.knowword;



import android.content.Intent;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
        import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Dashboard extends AppCompatActivity {
    String id;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListner;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Button button = (Button) findViewById(R.id.signout);
        mAuth = FirebaseAuth.getInstance();



        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser()==null)
                {
                    startActivity(new Intent(Dashboard.this, Home_Screen.class));
                }
               id= firebaseAuth.getCurrentUser().getUid();

            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                signOut();
                finish();





            }
        });




    }

    //sign out method
    public void signOut() {
        FirebaseAuth.getInstance().signOut();
    }

    public void close(View view)
    {
        System.exit(0);
    }
    public void addWord(View view) {

        Intent i = new Intent(this, AddWord.class);
        startActivity(i);

    }

    public void study(View view) {

        Intent i = new Intent(this, Study.class);
        startActivity(i);

    }


    public void special(View view) {

        Intent i = new Intent(this, MyWords.class);
        startActivity(i);

    }
}
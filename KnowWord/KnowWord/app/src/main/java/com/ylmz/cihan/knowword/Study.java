package com.ylmz.cihan.knowword;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class Study extends AppCompatActivity {

    private ArrayList<String> arrayList;
    TextView studyTurksh;
    TextView studyEnglish;
    TextView studySentence;
    Button redbutton;
    String englishWord;
    String turkishWord;
    @Override
    public void onStart(){
        super.onStart();

        studyTurksh=(TextView)findViewById(R.id.studyTurkish);
        studyEnglish=(TextView)findViewById(R.id.studyEnglish);
        studySentence=(TextView)findViewById(R.id.studySentence);
        redbutton = (Button) findViewById(R.id.button);
        arrayList=new ArrayList<>();
        DatabaseReference ddRef= FirebaseDatabase.getInstance().getReference().child("words");
        turkishWord="çalışmak";
        redbutton.setText("study");
        studySentence.setText("i study english");
        redbutton.setBackgroundColor(Color.RED);
        ddRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    dataSnapshot.getKey();
                    Word a=ds.getValue(Word.class);
                    System.out.println(a.getTurkishWord());

                    arrayList.add(a.getTurkishWord()+" "+a.getEnglishWord()+" "+a.getSentence());

                }
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);
    }

    public void change(View view) {



        studyTurksh.setVisibility(View.INVISIBLE);
        int size=arrayList.size();

        if(size==0)
        {

            Toast.makeText(this,"Kelimeniz Yok",Toast.LENGTH_SHORT);

        }

        else{
            Random r=new Random(); //random sınıfı
            int a=r.nextInt(size);

            String nesne=arrayList.get(a);
            System.out.println("random sayi::"+a);
            String[] parts = nesne.split(" ");
            String cümle="";
            String bos=" ";
            for (int i=2;i<parts.length;i++)
            {
                cümle=cümle+parts[i]+bos;



            }
            redbutton.setBackgroundColor(Color.RED);
            System.out.println(parts[0]);
            System.out.println(parts[1]);
            System.out.println(parts[2]);
            studyTurksh.setText(parts[0]);
            studyEnglish.setText(parts[1]);
            studySentence.setText(cümle);
            englishWord=parts[1];
            turkishWord=parts[0];
            redbutton.setText(englishWord);

        }




    }
    public void show(View view) {
        redbutton.setText(turkishWord);
        redbutton.setBackgroundColor(Color.GREEN);
        studyTurksh.setVisibility(View.VISIBLE);

    }


}

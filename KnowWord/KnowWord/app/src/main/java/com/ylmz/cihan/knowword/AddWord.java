package com.ylmz.cihan.knowword;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddWord extends AppCompatActivity {

    String idd;
    FirebaseUser currentUser=FirebaseAuth.getInstance().getCurrentUser();
    FirebaseAuth.AuthStateListener mAuthListner;
    FirebaseDatabase db =FirebaseDatabase.getInstance();
    DatabaseReference myRef=db.getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);

        System.out.print("gelenntr::");
    }

    public void add(View view) {
        EditText turkishWord=(EditText)findViewById(R.id.turkishWord);
        EditText englishWord=(EditText)findViewById(R.id.englishWord);
        EditText sentence=(EditText)findViewById(R.id.sentence);
        String turkish= turkishWord.getText().toString().replace(" ", "");;
        String english= englishWord.getText().toString();
        String sentence2= sentence.getText().toString();
        System.out.print("gelenntr::"+turkish);
       idd=currentUser.getUid();



       if(turkishWord.getText().toString().trim().length() == 0  )
       {

           Toast.makeText(this,"LÜtfen Türkçe Kelime Giriniz!",Toast.LENGTH_SHORT).show();

       }

        if(sentence.getText().toString().length()==0 )
        {
            Toast.makeText(this,"LÜtfen Cümle Yazınız ",Toast.LENGTH_SHORT).show();

        }

         if(englishWord.getText().toString().length()==0)
        {

            Toast.makeText(this,"LÜtfen İngilizce Kelime Yazınız!",Toast.LENGTH_SHORT).show();
        }


        if(turkishWord.getText().toString().trim().length() != 0 && sentence.getText().toString().length()!=0 &&englishWord.getText().toString().length()!=0)
        {
             //turkish.replace(" ","");
           Word addWord=new Word(turkish,english,sentence2,idd);

           myRef.child("words").push().setValue(addWord);
           Intent i = new Intent(this, Dashboard.class);
           startActivity(i);


       }







    }


}

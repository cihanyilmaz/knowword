package com.ylmz.cihan.knowword;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class MyWords extends AppCompatActivity {
    FirebaseAuth.AuthStateListener mAuthListner;
    String idd;
    FirebaseUser currentUser=FirebaseAuth.getInstance().getCurrentUser();
    private ArrayList<String> arrayList;
    TextView studyTurksh;
    TextView studyEnglish;
    TextView studySentence;
    boolean result=false;
    String uidd;

    @Override
    public void onStart(){
        super.onStart();
        idd=currentUser.getUid();
        studyTurksh=(TextView)findViewById(R.id.studyTurkish);
        studyEnglish=(TextView)findViewById(R.id.studyEnglish);
        studySentence=(TextView)findViewById(R.id.studySentence);
        arrayList=new ArrayList<>();
        DatabaseReference ddRef= FirebaseDatabase.getInstance().getReference().child("words");

        ddRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    dataSnapshot.getKey();
                    Word a=ds.getValue(Word.class);
                    System.out.println(a.getTurkishWord());


                    uidd=a.getUid();
                    System.out.println("uzunluk1:::"+idd.toString().trim().length());
                    System.out.println("uzunluk2:::"+a.getUid().trim().length());
                   if(idd.replace(" ", "")==uidd.replace(" ", ""))
                    {
                        System.out.println("ife girdii:::");
                        arrayList.add(a.getTurkishWord()+" "+a.getEnglishWord()+" "+a.getSentence());
                    }





                }
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_words);
    }

    public void change(View view) {



        studyTurksh.setVisibility(View.INVISIBLE);
        int size=arrayList.size();

        if(size==0)
        {

            Toast.makeText(this,"Kelimeniz Yok",Toast.LENGTH_SHORT);

        }

        else{
            Random r=new Random(); //random sınıfı
            int a=r.nextInt(size);

            String nesne=arrayList.get(a);
            System.out.println("random sayi::"+a);
            String[] parts = nesne.split(" ");
            String cümle="";
            String bos=" ";
            for (int i=2;i<parts.length;i++)
            {
                cümle=cümle+parts[i]+bos;



            }

            System.out.println(parts[0]);
            System.out.println(parts[1]);
            System.out.println(parts[2]);
            studyTurksh.setText(parts[0]);
            studyEnglish.setText(parts[1]);
            studySentence.setText(cümle);

        }




    }

    public void show(View view) {

        studyTurksh.setVisibility(View.VISIBLE);

    }

}
